use crossbeam_utils::atomic::AtomicCell;
use once_cell::sync::Lazy;

#[derive(Clone, Debug)]
pub struct CacheStats {
    pub estimated_entry_count: u64,
    pub weighted_size: u64,
    pub freq_sketch_size: u64, // bytes
    pub hashmap_size: u64,     // bytes
    // pub insertion_count: u64,
    // pub admission_count: u64,
    // pub rejection_count: u64,
    // pub eviction_count: u64,
}

impl CacheStats {
    pub(crate) fn new(
        estimated_entry_count: u64,
        weighted_size: u64,
        hashmap_size: u64,
        freq_sketch_size: u64,
        // insertion_count: u64,
        // admission_count: u64,
        // rejection_count: u64,
        // eviction_count: u64,
    ) -> Self {
        Self {
            estimated_entry_count,
            weighted_size,
            freq_sketch_size,
            hashmap_size,
            // insertion_count,
            // admission_count,
            // rejection_count,
            // eviction_count,
        }
    }
}

#[derive(Clone, Debug)]
pub struct GlobalStats {
    pub value_entry_creation_count: u64,
    pub value_entry_drop_count: u64,
    pub entry_info_creation_count: u64,
    pub entry_info_drop_count: u64,
    pub aoq_node_creation_count: u64,
    pub aoq_node_drop_count: u64,
    pub woq_node_creation_count: u64,
    pub woq_node_drop_count: u64,
}

impl GlobalStats {
    pub fn current() -> Self {
        let c = &GLOBAL_COUNTERS;
        Self {
            value_entry_creation_count: c.value_entry_creation_count.load(),
            value_entry_drop_count: c.value_entry_drop_count.load(),
            entry_info_creation_count: c.entry_info_creation_count.load(),
            entry_info_drop_count: c.entry_info_drop_count.load(),
            aoq_node_creation_count: c.aoq_node_creation_count.load(),
            aoq_node_drop_count: c.aoq_node_drop_count.load(),
            woq_node_creation_count: c.woq_node_creation_count.load(),
            woq_node_drop_count: c.woq_node_drop_count.load(),
        }
    }
}

pub(crate) static GLOBAL_COUNTERS: Lazy<GlobalCounters> = Lazy::new(Default::default);

#[derive(Default)]
pub(crate) struct GlobalCounters {
    value_entry_creation_count: AtomicCell<u64>,
    value_entry_drop_count: AtomicCell<u64>,
    entry_info_creation_count: AtomicCell<u64>,
    entry_info_drop_count: AtomicCell<u64>,
    aoq_node_creation_count: AtomicCell<u64>,
    aoq_node_drop_count: AtomicCell<u64>,
    woq_node_creation_count: AtomicCell<u64>,
    woq_node_drop_count: AtomicCell<u64>,
}

impl GlobalCounters {
    pub(crate) fn incl_value_entry_creation_count(&self) {
        self.value_entry_creation_count.fetch_add(1);
    }

    pub(crate) fn incl_value_entry_drop_count(&self) {
        self.value_entry_drop_count.fetch_add(1);
    }

    pub(crate) fn incl_entry_info_creation_count(&self) {
        self.entry_info_creation_count.fetch_add(1);
    }

    pub(crate) fn incl_entry_info_drop_count(&self) {
        self.entry_info_drop_count.fetch_add(1);
    }

    pub(crate) fn incl_aoq_node_creation_count(&self) {
        self.aoq_node_creation_count.fetch_add(1);
    }

    pub(crate) fn incl_aoq_node_drop_count(&self) {
        self.aoq_node_drop_count.fetch_add(1);
    }

    pub(crate) fn incl_woq_node_creation_count(&self) {
        self.woq_node_creation_count.fetch_add(1);
    }

    pub(crate) fn incl_woq_node_drop_count(&self) {
        self.woq_node_drop_count.fetch_add(1);
    }
}
